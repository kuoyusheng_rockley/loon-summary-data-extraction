#!/usr/bin/env python
# coding: utf-8

# In[2]:


import pandas as pd
import numpy as np
import os
import h5py
import time
import uuid


# In[93]:





# In[80]:


def get_sli_wavelenth_smsr(sli_filename, target_current = 150):
    
    #filename = r'C:\Users\yu-sheng.kuo\Desktop\PIV_master_file\hdf5\SLI_B39564A_R1C3_D12_CH0_20220223_192755.h5'
    f = h5py.File(sli_filename, "r")
    if list(f.keys())[0] != "SLI":
        raise Exception("Error!, {} is not sli file".format(sli_filename))
    sli_df = pd.DataFrame(np.array(f["SLI"]["Results"]["Dataset"]))
    sli_df = sli_df[sli_df['current'] == target_current].reset_index()
    peak_lambda = sli_df.loc[sli_df['level'].argmax(), 'wavelength']
    sm_level = max(sli_df.loc[sli_df['level'].argmax()+35:, 'level'].max(),sli_df.loc[:sli_df['level'].argmax()-35, 'level'].max())
    results = {}
    results['peak_wavelength'] = {'value':peak_lambda, 'unit':f["SLI"]["Results"]["Dataset"].attrs['wavelength_units']}
    results['smsr'] = {'value':sli_df['level'].max()-sm_level, 'unit':'dB'}
    return results


# In[113]:


def get_Isat(piv_filename , sat_v= -3.5):
    #filename = r'C:\Users\yu-sheng.kuo\Desktop\PIV_master_file\hdf5\SLI_B39564A_R1C3_D12_CH0_20220223_192755.h5'
    f = h5py.File(piv_filename, "r")
    #print(f.keys())
    if list(f.keys())[0] != 'TX_PIV':
        raise Exception("Error!, {} is not Tx_PIV file".format(piv_filename))
    piv_df = pd.DataFrame(np.array(f['TX_PIV']["Results"]["Dataset"]))
    results = {}
    results['Isat_{}V'.format(sat_v)] = {'value':piv_df['eam_current'][piv_df['eam_voltage'] == sat_v].mean(), 'unit':f['TX_PIV']["Results"]["Dataset"].attrs['eam_current_units']}
    return results

def get_mpd_Isat_ratio(piv_filename, sat_v = -3.5):
    f = h5py.File(piv_filename, "r")
    #print(f.keys())
    if list(f.keys())[0] != 'TX_PIV':
        raise Exception("Error!, {} is not Tx_PIV file".format(piv_filename))
    piv_df = pd.DataFrame(np.array(f['TX_PIV']["Results"]["Dataset"]))
    results = {}
    results['Isat/MPD'] = {'value':piv_df['eam_current'][piv_df['eam_voltage'] == sat_v].mean()/piv_df['mpd_current'][piv_df['eam_voltage'] == sat_v].mean()
                          ,'unit':''
                          }
    return results


# In[107]:


def get_DFB_threshold(liv_filename, threshold = 1e-1, eam_voltage = 0):
    f = h5py.File(liv_filename, "r")
    if list(f.keys())[0] != 'TX_LIV':
            raise Exception("Error!, {} is not Tx_LIV file".format(liv_filename))
    liv_df = pd.DataFrame(np.array(f['TX_LIV']["Results"]["Dataset"]))
    liv_df = liv_df[(liv_df['eam_voltage'] == eam_voltage) & (liv_df['power'] < threshold)].reset_index()
    liv_df['current'].max()
    results = {}
    results['liv_threshold'] = {'value':liv_df['current'].max(), 
                                'unit':f['TX_LIV']["Results"]["Dataset"].attrs['current_units']}
    return results


# In[108]:


def get_liv_summary_data(liv_filename, threshold = 60, eam_voltage = 0):
    f = h5py.File(liv_filename, "r")
    if list(f.keys())[0] != 'TX_LIV':
            raise Exception("Error!, {} is not Tx_LIV file".format(liv_filename))
    liv_df = pd.DataFrame(np.array(f['TX_LIV']["Results"]["Dataset"]))
    liv_df = liv_df[(liv_df['eam_voltage'] == eam_voltage) & (liv_df['current']>threshold) ].reset_index()
    results = {}

    results['{}V_liv_max_power'.format(eam_voltage)] = {'value':liv_df['power'].max(), 
                                                        'unit':f['TX_LIV']["Results"]["Dataset"].attrs['power_units']}
    results['{}V_liv_max_DFB'.format(eam_voltage)] = {'value':liv_df.loc[liv_df['power'].argmax(), 'current'],
                                                      'unit':f['TX_LIV']["Results"]["Dataset"].attrs['current_units']}
    results['{}V_liv_max_eam'.format(eam_voltage)] = {'value':liv_df.loc[liv_df['power'].argmax(),'eam_current'],
                                                     'unit':f['TX_LIV']["Results"]["Dataset"].attrs['eam_current_units']}
    return results

if __name__ == "__main__" :
    # filename = r"C:\Users\yu-sheng.kuo\Desktop\LIV_ADJUST_TEST_R1C3_D12_CH0_20220223_192550.h5"
    filename = r'C:\Users\yu-sheng.kuo\Desktop\PIV_master_file\hdf5\SLI_B39564A_R1C3_D12_CH0_20220223_192755.h5'
    f = h5py.File(filename, "r")
    piv_filename = r"C:\Users\yu-sheng.kuo\Desktop\PIV_master_file\hdf5\TX_PIV_B39564A_R1C3_D12_CH0_20220223_192632.h5"
    liv_filename = r"C:\Users\yu-sheng.kuo\Desktop\PIV_master_file\hdf5\TX_LIV_B39564A_R1C3_D12_CH0_20220223_192550.h5"
    liv_summary  = get_liv_summary_data(liv_filename)